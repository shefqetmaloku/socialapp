package com.tahirietrit.socialapp.api;

import com.tahirietrit.socialapp.model.LoginResponse;
import com.tahirietrit.socialapp.model.feed.FeedResponse;
import com.tahirietrit.socialapp.model.feed.Post;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("/paintbook/index.php")
    Call<LoginResponse> loginUser(@Query("User") String user,
                                  @Query("Password") String password);


    @GET("/paintbook/index.php")
    Call<FeedResponse> getFeed(@Query("GetPostet") String postCount,
                               @Query("UserID") String userId);

//    @GET("/paintbook/index.php")
//    Call<List<Post>> getPosts(@Query("GetPostet") String postCount,
//                             @Query("UserID") String userId,
//                              @Query("Time") String getCreatedDate,
//                              @Query("Photo") String getPhotoUrl);
}
//wifi: auk-campus
//pass: ritkosova
