package com.tahirietrit.socialapp.ui;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tahirietrit.socialapp.R;
import com.tahirietrit.socialapp.model.feed.FeedResponse;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    Intent intent;
    FeedResponse feedResponse;

    public ListAdapter(Context ctx, LayoutInflater inflater) {
        this.ctx = ctx;
        this.inflater = inflater;
        feedResponse = new FeedResponse();
    }

    public void setIntent(Intent intent) {

        this.intent = intent;
    }

    public void setFeedResponse(FeedResponse feedResponse) {
        this.feedResponse = feedResponse;
    }

    @Override
    public int getCount() {
        return feedResponse.postet != null ? feedResponse.postet.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return feedResponse.postet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView==null)
        {
            convertView =inflater.inflate(R.layout.newsfeed,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        else
        {
            holder  = (ViewHolder)convertView.getTag();
        }
        holder.username.setText(feedResponse.postet.get(i).getUsername());
        holder.activity.setText(feedResponse.postet.get(i).getDateDifference());
        return convertView;
    }
}

class ViewHolder
{
    TextView username;
    TextView activity;

    public ViewHolder(View view)
    {
        username=view.findViewById(R.id.username_textview);
        activity=view.findViewById(R.id.activity_textview);
    }
}
