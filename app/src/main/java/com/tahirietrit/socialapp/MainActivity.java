package com.tahirietrit.socialapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.tahirietrit.socialapp.api.ApiService;
import com.tahirietrit.socialapp.api.Servicefactory;
import com.tahirietrit.socialapp.model.feed.FeedResponse;
import com.tahirietrit.socialapp.prefs.AppPreferences;
import com.tahirietrit.socialapp.ui.ListAdapter;
import com.tahirietrit.socialapp.ui.NewsFeed;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.list_view);

        adapter = new ListAdapter(MainActivity.this, getLayoutInflater());
        Intent intent = new Intent(MainActivity.this,NewsFeed.class);
        adapter.setIntent(intent);
        listView.setAdapter(adapter);

        getFeed();
    }



    private void getFeed() {
        ApiService apiService = Servicefactory.retrofit.create(ApiService.class);
        Call<FeedResponse> call = apiService.getFeed("",
                AppPreferences.getUserid());
        call.enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                adapter.setFeedResponse(response.body());
                listView.setAdapter(adapter);
                System.out.println("feed count " + response.body().postet.size());

                if (!response.isSuccessful())
                {

                }

                else
                {

                }

            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Wrong combination",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
