package com.tahirietrit.socialapp.model.feed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Post {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("photo_url")
    @Expose
    private String photoUrl;
    @SerializedName("pershkrimi")
    @Expose
    private String pershkrimi;
    @SerializedName("created_date")
    @Expose
    private String createdDate;

    public Post(String userId, String username, String photoUrl) {
        this.userId = userId;
        this.username = username;
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPershkrimi() {
        return pershkrimi;
    }

    public void setPershkrimi(String pershkrimi) {
        this.pershkrimi = pershkrimi;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getDateDifference() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
//        dateFormat.setLenient(false);
        try {
            Date d1 = dateFormat.parse(createdDate);
            Date d2 = new Date();
            long diffInMillies = Math.abs(d2.getTime() - d1.getTime());
            long diff = TimeUnit.SECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diff / (24 * 60 * 60) > 0) // days
            {
                return (diff / (24 * 60 * 60)) + " days ago";
            }
            else if (diff / (60 * 60) > 0) // hours
            {
                return (diff / (60 * 60)) + " hours ago";
            }
            else if (diff / (60) > 0) // hours
            {
                return (diff / (60)) + " minutes ago";
            }
            else // seconds
            {
                return (diff) + " seconds ago";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


}